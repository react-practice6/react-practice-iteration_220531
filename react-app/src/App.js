import './App.css';
import Header from './component/Header/Header';
import Footer from './component/Footer/Footer';
import Body from './component/Body/Body';
import Description from './component/Description/Description';

function App() {
  return (
    <>
      <Header />
      <Body />
      <br></br>
      <br></br>
      <br></br>
      <Description />
      <Footer />
    </>
  );
}

export default App;
