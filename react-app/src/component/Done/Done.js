import React from 'react';
import '../common.css';
import './Done.css';

export default function Done() {

    const { useState } = React;

    const [list, setList] = useState([]);
    const [inputText, setInputText] = useState("");
    const [nextId, setNextId] = useState(list.length + 1);
    let checked = [];

    const onChangeHandler = e => setInputText(e.target.value);
    const onClickHandler = () => {
        const changedList = list.concat({
            id: nextId,
            name: inputText
        });

        setNextId(nextId + 1);
        setInputText("");
        setList(changedList);
    };
    const onCheckHandler = (e) => {
        checked.push(e.target.value);
    };
    const onRemove = id => {
        const changedList = list.filter(el => el.id !== id);
        setList(changedList);
    };

    const doneList = list.map(el => 
        <div className="oneDoneList" key={ el.id } onDoubleClick={ () => onRemove(el.id) }>
            <input className="doneListCheckbox" type="checkbox" value={ el.id } onClick={ onCheckHandler }></input>
            <div>{ el.name }</div>
        </div>
    );

    return (
        <div id="doneListArea">
            <div className="contentTitle">ToDoList</div>
            <br></br>
            <div className="contentArea">
                { doneList }
            </div>
            <br></br>
            <input id="addDoneList" placeholder='추가할 할 일' value={ inputText || "" } onChange={ onChangeHandler }/>
            <button id="addDoneListBtn" onClick={ onClickHandler }>추가</button>
        </div>
    );
}