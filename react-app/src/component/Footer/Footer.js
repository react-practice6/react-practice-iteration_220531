import './Footer.css';

export default function Footer() {

    return (
        <div id="copyright">Copyright © TJ Lee's ToDoList 2022</div>
    );
}