import './Body.css';
import ToDoList from '../ToDoList/ToDoList';
import Done from '../Done/Done';

export default function Body() {

    return (
        <div id="container">
            <ToDoList />
            <Done />
        </div>
    );
}