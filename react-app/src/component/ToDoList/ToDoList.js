import React from 'react';
import '../common.css';
import './ToDoList.css';

export default function ToDoList() {

    const { useState } = React;

    const [list, setList] = useState([]);
    const [inputText, setInputText] = useState("");
    const [nextId, setNextId] = useState(list.length + 1);

    const onChangeHandler = e => setInputText(e.target.value);
    const onClickHandler = () => {
        const changedList = list.concat({
            id: nextId,
            name: inputText
        });

        setNextId(nextId + 1);
        setInputText("");
        setList(changedList);
    };

    const toDoList = list.map(el => 
        <div className="oneToDoList" key={ el.id }>
            <input className="toDoListCheckbox" type="checkbox" value={ el.id }></input>
            <div>{ el.name }</div>
        </div>
    );

    return (
        <div id="toDoListArea">
            <div className="contentTitle">ToDoList</div>
            <br></br>
            <div className="contentArea">
                { toDoList }
            </div>
            <br></br>
            <input id="addToDoList" placeholder='추가할 할 일' value={ inputText || "" } onChange={ onChangeHandler }/>
            <button id="addToDoListBtn" onClick={ onClickHandler }>추가</button>
        </div>
    );
}